import { Component, OnInit, Input } from '@angular/core';
import { Photographer } from '../photographer';
import { PhotographerService } from '../services/photographer.service';

@Component({
    selector: 'photo-list',
    templateUrl : 'app.photolist.html',
    providers: [PhotographerService]
})

export class PhotoListComponent {
    title = 'Photographers List';
    photographers: Photographer[];
    photoSelected: Photographer;

constructor(private photographerService: PhotographerService) { }
 
  getPhotographers(): void {
    this.photographerService.getPhotographers().then(photographer => this.photographers = photographer);
  }
 
  ngOnInit(): void {
    this.getPhotographers();
  }
 
  onSelect(photographer: Photographer): void {
    this.photoSelected = photographer;
  }
}