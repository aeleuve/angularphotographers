import { Injectable } from '@angular/core';

import { Photographer } from '../photographer';
import { PHOTOGRAPHERS } from '../mocks/mock.photographers';

@Injectable()
export class PhotographerService {
    getPhotographers(): Promise<Photographer[]>{
        return Promise.resolve(PHOTOGRAPHERS);
    }
}