export class Photographer {
  id: number;
  name: string;
  bio: string;
  image: string;
}