import { AlvaroAppPage } from './app.po';

describe('alvaro-app App', () => {
  let page: AlvaroAppPage;

  beforeEach(() => {
    page = new AlvaroAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
